import unittest
from prog import clear,transfodico,transfodicoetindex
#faire le bon import 

class TestProgFunctions(unittest.TestCase):
    # test de la fonction clear :


    # test de la fonction de formatage du dictionnaire des entêtes


    #test de la fonction de creation de la table et gestion des erreurs liees



class TestMainFunctions(unittest.TestCase):
    def test_clear(self):
        self.assertEqual(clear("select * from test where papi=test;"),"select * from test where papi=test ")
        self.assertEqual(clear("-- je suis un commentaire et je nique ta mere"),"  je suis un commentaire et je nique ta mere")
    def test_transfodico(self):
        dictiotowrite = {
            'address_titulaire': clear("test"),
            'nom': clear("nom"),
            'prenom':clear("prenom"),
            'immatriculation':clear('immatriculation'),
            'date_immatriculation':clear('date_immatriculation') ,
            'vin':clear('vin') ,
            'marque': clear('marque') ,
            'denomination_commerciale': clear('denomination_commerciale') ,
            'couleur': clear('couleur') ,
            'carrosserie': clear('carrosserie'),
            'categorie': clear('categorie') ,
            'cylindree': clear('cylindree') ,
            'energie': clear('energie') ,
            'places': clear('places') ,
            'poids': clear('poids') ,
            'puissance': clear('puissance') ,
            'type': clear('type') ,
            'variante': clear('variante') ,
            'version': clear('version') 
        }
        dictiotowrite2 = {
            'address_titulaire': "test",
            'nom': "nom",
            'prenom':"prenom",
            'immatriculation':'immatriculation',
            'date_immatriculation':'date_immatriculation' ,
            'vin':'vin' ,
            'marque': 'marque' ,
            'denomination_commerciale': 'denomination_commerciale' ,
            'couleur': 'couleur' ,
            'carrosserie': 'carrosserie',
            'categorie': 'categorie' ,
            'cylindree': 'cylindree' ,
            'energie': 'energie' ,
            'places': 'places' ,
            'poids': 'poids' ,
            'puissance': 'puissance' ,
            'type': 'type' ,
            'variante': 'variante' ,
            'version': 'version' 
        }
        self.assertEqual(transfodico(dictiotowrite2),dictiotowrite)
        self.assertEqual(transfodico(dictiotowrite),dictiotowrite2)

    def test_transfodico(self):
        dictiotowrite = {
            'address_titulaire': clear("test"),
            'nom': clear("nom"),
            'prenom':clear("prenom"),
            'immatriculation':clear('immatriculation'),
            'date_immatriculation':clear('date_immatriculation') ,
            'vin':clear('vin') ,
            'marque': clear('marque') ,
            'denomination_commerciale': clear('denomination_commerciale') ,
            'couleur': clear('couleur') ,
            'carrosserie': clear('carrosserie'),
            'categorie': clear('categorie') ,
            'cylindree': clear('cylindree') ,
            'energie': clear('energie') ,
            'places': clear('places') ,
            'poids': clear('poids') ,
            'puissance': clear('puissance') ,
            'type': clear('type') ,
            'variante': clear('variante') ,
            'version': clear('version') 
        }
        dictiotowrite2 = {
            'address_titulaire': "test",
            'nom': "nom",
            'prenom':"prenom",
            'immatriculation':'immatriculation',
            'date_immatriculation':'date_immatriculation' ,
            'vin':'vin' ,
            'marque': 'marque' ,
            'denomination_commerciale': 'denomination_commerciale' ,
            'couleur': 'couleur' ,
            'carrosserie': 'carrosserie',
            'categorie': 'categorie' ,
            'cylindree': 'cylindree' ,
            'energie': 'energie' ,
            'places': 'places' ,
            'poids': 'poids' ,
            'puissance': 'puissance' ,
            'type': 'type' ,
            'variante': 'variante' ,
            'version': 'version' ,
            'index' : 12
        }
        self.assertEqual(transfodicoetindex(dictiotowrite2,12),dictiotowrite)
        
        