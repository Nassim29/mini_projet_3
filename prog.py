import sqlite3
import logging
import csv
from copy import copy
import unicodedata
import sys
from datetime import datetime
import os.path
import os

caracteres_combinaison = dict.fromkeys(c for c in range(sys.maxunicode)
if unicodedata.combining(chr(c)))

log_path=('./log/') #emplacement des logs
db_path=('./test2.sqlite3') #emplacement de la DB
csv_path=('./test.csv') #emplacement du csv

def erreur(error,connection): # fonction appellee en cas d'erreur dans la DB
    connection.rollback()
    connection.close()
    logging.warning("Erreur retour a son état d'origine de la bdd et fermeture de celle ci, le programme va ce fermer"+error)

def clear(test): #fonction pour nettoyer un text pour envoi dans la DB
    new= test.replace(";"," ").replace("--", " ").replace('"',' ').replace("drop"," ").replace("DROP"," ")
    new = unicodedata.normalize('NFKD', new).encode('ascii','ignore')
    logging.debug("traduction de %s en %s",test,new)
    return new.decode("ascii")

def transfodico(row): # Création d'un dico "propre"
    logging.debug("Traduction et nettoyage des variable d'un dictionnaire")
    dictiotowrite = {
            'address_titulaire': clear(row['address_titulaire']),
            'nom': clear(row['nom']),
            'prenom':clear(row['prenom']),
            'immatriculation':clear(row['immatriculation']),
            'date_immatriculation':clear(row['date_immatriculation']) ,
            'vin':clear(row['vin']) ,
            'marque': clear(row['marque']) ,
            'denomination_commerciale': clear(row['denomination_commerciale']) ,
            'couleur': clear(row['couleur']) ,
            'carrosserie': clear(row['carrosserie']),
            'categorie': clear(row['categorie']) ,
            'cylindree': clear(row['cylindree']) ,
            'energie': clear(row['energie']) ,
            'places': clear(row['places']) ,
            'poids': clear(row['poids']) ,
            'puissance': clear(row['puissance']) ,
            'type': clear(row['type']) ,
            'variante': clear(row['variante']) ,
            'version': clear(row['version']) 
        }
    return dictiotowrite   
def transfodicoetindex(row,strin): #création d'un dico clean avec un index en plus
    logging.debug("Traduction et nettoyage des variable d'un dictionnaire")
    dictiotowrite = {
            'address_titulaire': clear(row['address_titulaire']),
            'nom': clear(row['nom']),
            'prenom':clear(row['prenom']),
            'immatriculation':clear(row['immatriculation']),
            'date_immatriculation':clear(row['date_immatriculation']) ,
            'vin':clear(row['vin']) ,
            'marque': clear(row['marque']) ,
            'denomination_commerciale': clear(row['denomination_commerciale']) ,
            'couleur': clear(row['couleur']) ,
            'carrosserie': clear(row['carrosserie']),
            'categorie': clear(row['categorie']) ,
            'cylindree': clear(row['cylindree']) ,
            'energie': clear(row['energie']) ,
            'places': clear(row['places']) ,
            'poids': clear(row['poids']) ,
            'puissance': clear(row['puissance']) ,
            'type': clear(row['type']) ,
            'variante': clear(row['variante']) ,
            'version': clear(row['version']) ,
            'index' : strin
        }
    return dictiotowrite       

def Creatab(): #Fonction pour creer le lien avec la DB a la quelle on va ajouter la/les table(s)
    try:
        connection = sqlite3.connect(db_path)
        cursor =connection.cursor()
        cursor.execute("""CREATE TABLE tuture ( address_titulaire TEXT,nom TEXT,
        prenom TEXT,
        immatriculation TEXT,
        date_immatriculation TEXT,
        vin TEXT,
        marque TEXT,
        denomination_commerciale TEXT,
        couleur TEXT,
        carrosserie TEXT,
        categorie TEXT,
        cylindree TEXT,
        energie TEXT,
        places TEXT,
        poids TEXT,
        puissance TEXT,
        variante TEXT,
        version TEXT,
        type TEXT,
        ind INTEGER PRIMARY KEY AUTOINCREMENT
        );
        """)
        connection.commit()
        return cursor, connection
    except sqlite3.Error as identifier:
        erreur(identifier,connection)

def TestExistence_DB(): #test si la DataBase existe

    if os.path.exists(db_path): #test si le fichier de DB existe
        try:
            connection = sqlite3.connect(db_path)
            cursor =connection.cursor()
        except sqlite3.Error as identifier:
            erreur(identifier,connection)
    else: #sinon on cree la db et la tab
        cursor,connection=Creatab()
    return cursor,connection;

def TestExistence_log(): #Test si le dossier de log existe
    if os.path.exists(log_path): #Test si l'élément log_path existe
        if os.path.isdir(log_path): #test si c'est un dossier
            file_name = (log_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        else: #sinon on arrete tout
            print("Erreur log est un fichier")
            exit(1)
    else: #sinon on le cree
        try:
            os.mkdir(log_path)
            file_name = (log_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        except OSError as error:
                print(error.__cause__)
                exit(1)
    return file_name;

def TestExistence_CSV(): #Test si le dossier de log existe
    if os.path.exists(csv_path): #Test si l'élément log_path existe
        return(0);
    else: #sinon on le cree
        exit(1);

def Bdd_Update(cursor,dico):
    cursor.execute("UPDATE tuture SET address_titulaire = :address_titulaire, nom = :nom,prenom = :prenom,immatriculation = :immatriculation,date_immatriculation = :date_immatriculation,vin = :vin,marque = :marque,denomination_commerciale = :denomination_commerciale,couleur = :couleur,carrosserie = :carrosserie,categorie = :categorie,cylindree = :cylindree,energie = :energie,places = :places,poids = :poids,puissance = :puissance,variante = :variante ,version = :version, type = :type WHERE ind = :index ", dico)
    return

def Bdd_insert(cursor,dico):
    cursor.execute("insert into tuture (address_titulaire,nom, prenom ,immatriculation,date_immatriculation,vin,marque ,denomination_commerciale ,couleur,carrosserie , categorie,cylindree, energie,places , poids , puissance , variante , version , type ) values(:address_titulaire,:nom, :prenom ,:immatriculation,:date_immatriculation,:vin,:marque ,:denomination_commerciale ,:couleur,:carrosserie , :categorie,:cylindree, :energie,:places , :poids , :puissance , :variante , :version , :type )", dico)


def main():
    cursor,connection =TestExistence_DB();
    file_name=TestExistence_log();
    TestExistence_CSV();
    logging.basicConfig(format ='%(asctime)s : %(levelname)s : import.py : %(message)s',filename=file_name,level=logging.DEBUG) #configuration du module logging
    logging.info("Début du programme") #création d'un point de début de prog
    with open(csv_path) as csvfile: 
        logging.info("ouverture du fichier")
        reader = csv.DictReader(csvfile,delimiter=';')
        logging.info("délimiter choisis")
        for row in reader:
            if(len(row)>=20):
                logging.info("Jump de la ligne courrante " + row.__str__())
            else:
                cursor.execute("select * from tuture where immatriculation=?",(clear(row['immatriculation']),))
                logging.info("Test si %s existe dans la table tuture",row['immatriculation'])
                boucle=0
                for rowc in cursor.fetchall():
                    boucle=boucle+1
                    test= rowc[19]
                if(boucle==1):
                    logging.info("Présent %d  fois",boucle)
                    Bdd_Update(cursor,transfodicoetindex(row,test))
                    logging.info("mise a jour des valeur avec index %d",test)
                elif boucle>1:
                    logging.info("Présent %d  fois",boucle)
                    erreur(" une ligne est présente + d'une fois entrer en erreur",connection)
                else:
                    logging.info("data non présente, insertion dans la bdd")
                    Bdd_insert(cursor,transfodico(row))
                    logging.info("Ecriture de " + transfodico(row).__str__() + "dans la bdd" )
                    logging.info("écriture aux totale de" + boucle.__str__())
    connection.commit()
    logging.debug("Changement envoyer")
    connection.close()
    logging.debug("BDD fermer")



if __name__ =='__main__': #si le programme est le main lancement
    main();
